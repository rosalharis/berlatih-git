<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function form(){
        return view('halaman.form');
    }

    public function register(Request $request){
        $firstname = $request->firstname;
        $lastname = $request->lastname;

        return view('halaman.home', compact('firstname', 'lastname'));
    }

}
