@extends('layout.master')
@section('judul')
    Halaman Edit Cast id {{$cast->id}}
@endsection
@section('content')

<form action="/cast/{{$cast->id}}" method="POST">
  @csrf
  @method('put')
  <div class="form-group">
      <label for="nama">Nama</label>
      <input type="text" class="form-control" name="nama" value="{{$cast->nama}}" id="umur" placeholder="Masukkan Nama Pemeran">
      @error('nama')
          <div class="alert alert-danger">
              {{ $message }}
          </div>
      @enderror
  </div>
  <div class="form-group">
      <label for="umur">Umur</label>
      <input type="text" class="form-control" name="umur" value="{{$cast->umur}}" id="umur" placeholder="Masukkan Umur Pemeran">
      @error('umur')
          <div class="alert alert-danger">
              {{ $message }}
          </div>
      @enderror
  </div>
  <div class="form-group">
    <label for="bio">Bio</label>
    <textarea class="form-control" name="bio" id="bio" cols="30" rows="10">{{$cast->bio}}</textarea>
    @error('bio')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
    @enderror
</div>
  <button type="submit" class="btn btn-primary">Update</button>
</form>

@endsection