@extends('layout.master')
@section('judul')
    <h1>SanberBook - Social Media Developer</h1>
    <p>Belajar dan Berbagi agar hidup ini semakin santai berkualitas</p>
@endsection
@section('content')
    <h3>Benefit Join di SanberBook</h3>
    <ul>
        <li>Mendapatkan motivasi dari sesama developer</li>
        <li>Sharing knowledge dari para mastah developer</li>
        <li>Dibuat oleh calon web developer terbaik</li>
    </ul>

    <h3>Cara Bergabung ke SanberBook</h3>

    <ol>
        <li>Mengunjungi website ini</li>
        <li>Mendaftar di <a href="/form">Form Sign Up</a></li>
        <li>Selesai!</li>
    </ol>
@endsection
    

    