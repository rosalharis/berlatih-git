@extends('layout.master')
@section('judul')
    Halaman Form
@endsection
@section('content')
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>

    <form action="/register" method="POST">
        @csrf
        <p>First Name</p>
        <input type="text" name="firstname">
        <p>Last Name</p>
        <input type="text" name="lastname">
        <br><br>

        <label for="gender">Gender:</label>
        <br><br>
        <input type="radio" name="gender" id="gender" value="male">
        <label for="female">Male</label>
        <br>
        <input type="radio" name="gender" id="gender" value="female">
        <label for="female">Female</label>
        <br>
        <input type="radio" name="gender" id="gender" value="other">
        <label for="other">Other</label>
        <br><br>

        <label for="nationality">Nationality:</label><br><br>
        <select id="nationality" name="nationality">
            <option value="Indonesia">Indonesia</option>
            <option value="Singapore">Singapore</option>
            <option value="Malaysia">Malaysia</option>
            <option value="Japan">Japan</option>
        </select>
        <br><br>

        <Label>Language Spoken</Label><br><br>
        <input type="checkbox" name="language" id="language">
        <label for="bahasa">Bahasa Indonesia</label><br>
        <input type="checkbox" name="language" id="language">
        <label for="english">English</label><br>
        <input type="checkbox" name="language" id="language">
        <label for="other">Other</label>
        <br><br>

        <label for="bio">Bio</label><br><br>
        <textarea id="bio" name="bio" rows="10" cols="30"></textarea><br>
        <input type="submit" value="Sign Up">
    </form>
@endsection
